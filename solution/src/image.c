#include "image.h"
#include "io.h"

#include <stdint.h>
#include <stdlib.h>


struct image create_image(uint64_t width, uint64_t height) {
    struct pixel *data = malloc(sizeof(struct pixel) * width * height);
    if (data == NULL) {
        print_err("allocating error");
        return (struct image){0};
    }
    struct image img = {
        .width = width,
        .height = height,
        .data = data
    };
    return img;
}

void free_image(struct image img) {
    free(img.data);
}
