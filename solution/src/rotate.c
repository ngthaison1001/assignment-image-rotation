#include "rotate.h"
#include "image.h"

struct image rotate(struct image const source) {
    struct image res = create_image(source.height, source.width);
    
    uint64_t rows = res.height;
    uint64_t cols = res.width;

    for (size_t r = 0; r < rows; r++) {
        for (size_t c = 0; c < cols; c++) {
            res.data[r * cols + c] = source.data[(cols - c - 1) * rows + r];
        }
    }
    return res;
}
