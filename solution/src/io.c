#include "io.h"

#include <stdio.h>

void print_err(const char* msg) {
    fprintf(stderr, "%s\n", msg);
}

FILE* file_open_read(const char* path) {
    return fopen(path, "rb");
}

FILE* file_open_write(const char* path) {
    return fopen(path, "wb");
}

void file_close(FILE* fp) {
    if (fp) fclose(fp);
}
