#include "bmp.h"
#include "image.h"
#include "io.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        print_err("Usage: ./image-transformer <source-image> <transformed-image>\n");
        return 1;
    }

    // read 
    struct bmp_image source_bmp = read_bmp(argv[1]);

    // rotate
    struct image source_image = bmp_to_image(source_bmp);
    struct image transformed_image = rotate(source_image);

    free_image(source_image);

    struct bmp_image transformed_bmp = image_to_bmp(transformed_image);

    // save 
    if (!write_bmp(argv[2], transformed_bmp)) {
        print_err("BMP can't saved successfully\n");
        return 1;
    }

    free_image(transformed_image);

    return 0;
}
