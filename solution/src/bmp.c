#include "bmp.h"

#include <stdbool.h>
#include <stdint.h>

#include "image.h"
#include "io.h"

static uint8_t bmp_header_get_padding(uint64_t width) {
    return width % 4;
}

static bool bmp_header_is_correct(struct bmp_header const header) {
    if (header.bfType != 0x4d42 
        && header.biSize != 40 
        && header.biPlanes != 1 
        && header.biBitCount != 24 
        && header.bOffBits == 54) return false;
    else return true;
}

struct bmp_image read_bmp(const char* path) {
    struct bmp_image bmp = {{0}};
    
    FILE* fp = file_open_read(path);
    if (fp == NULL) {
        print_err("Error file opening");
        return (struct bmp_image){{0}};
    }

    // check read header of bmp
    if(fread(&bmp.header, sizeof(struct bmp_header), 1, fp)  != 1 ) {
        file_close(fp);
        return (struct bmp_image){{0}};
    }

    if (!bmp_header_is_correct(bmp.header)) {
        file_close(fp);
        return (struct bmp_image){{0}};
    }

    // cap phat bo nho cho toan bo pixel 
    bmp.pixels = malloc(sizeof(struct pixel)*bmp.header.biHeight
                    *bmp.header.biWidth);
    if (bmp.pixels == NULL) {
        print_err("Error allocating\n");
        return (struct bmp_image){{0}};
    }

    uint8_t padding = bmp_header_get_padding(bmp.header.biWidth);
    size_t row_bytes;
     
    // doc file theo tung dong 
    for (size_t row = 0; row < bmp.header.biHeight; row++) {
        row_bytes = fread(&(bmp.pixels[row*bmp.header.biWidth]), 
                sizeof(struct pixel), bmp.header.biWidth, fp);
        
        // move cur_pointer to new position that added the numbers of padding
        int fseek_result = fseek(fp, padding, SEEK_CUR); 
        // fseek = 0 if successfully

        if (bmp.header.biWidth != row_bytes || fseek_result != 0) {
            file_close(fp);
            return (struct bmp_image){{0}};
        }
    }
    file_close(fp);
    return bmp;
}

struct image bmp_to_image(struct bmp_image const bmp) {
    struct image img = {
        .width = bmp.header.biWidth,
        .height = bmp.header.biHeight,
        .data = bmp.pixels
    };
    return img;
}

static struct bmp_header create_bmp_header(struct image const img) {
    uint32_t biSizeImage = sizeof(struct pixel) * img.width * img.height 
                        + img.height * bmp_header_get_padding(img.width);

    uint32_t bOffBits = sizeof(struct bmp_header);
                        
    uint32_t biFileSize = biSizeImage + bOffBits;

    struct bmp_header header = {
        19778, 
        biFileSize, 
        0, 
        bOffBits,
        40, 
        img.width, 
        img.height, 
        1, 
        24, 
        0, 
        biSizeImage, 
        0, 
        0, 
        0, 
        0
    };

    return header;
}

struct bmp_image image_to_bmp(struct image const img) {
    struct bmp_header header = create_bmp_header(img);

    struct bmp_image bmp = {
        .header=header, 
        .pixels=img.data
    };

    return bmp;
}

bool write_bmp(const char* path, struct bmp_image const bmp) {
    FILE* fp = file_open_write(path);

    // check file opening
    if (!fp) {
        file_close(fp);
        return false; 
    }

    // check header writing
    if(!fwrite(&bmp.header, sizeof(struct bmp_header), 1, fp)) {
        file_close(fp);
        return false;
    }

    uint8_t padding = bmp_header_get_padding(bmp.header.biWidth);

    size_t row_bytes;
    const uint64_t zero = 0;
    for (size_t row = 0; row < bmp.header.biHeight; row++) {
        row_bytes = fwrite(&(bmp.pixels[row*bmp.header.biWidth]), 
                sizeof(struct pixel), bmp.header.biWidth, fp);

        if (!fwrite(&zero, 1, padding, fp)) {
            file_close(fp);
            return false;
        }; 

        if (bmp.header.biWidth != row_bytes) {
            file_close(fp);
            return false;
        }   
    }
    file_close(fp);
    return true;
}
