#pragma once

#include <stdio.h>

void print_err(const char* msg);

FILE* file_open_read(const char* path);
FILE* file_open_write(const char* path);

void file_close(FILE* fp);
